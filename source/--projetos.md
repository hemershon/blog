---
layout: page
title: Projetos
permalink: /projetos/
---
<div class="items row">
            <div class="card-deck">
              <div class="card">
                <img class="card-img-top" src="/images/pabx.png" alt="Calline logo" width="190" height="150">
                <div class="card-body">
                  <h4 class="card-title"><b>FALPBX Sistema administrativo</b></h4>
                  <p class="card-text">PBX da FAL v2</p>
                  <a href="https://github.com/hemershon/FALPBX" class="btn btn-primary">SAIBA MAIS</a>
                </div>
              </div>
            </div>
            <p>
<div class="items row">
            <div class="card-deck">
              <div class="card">
                <img class="card-img-top" src="/images/pessoas.png" alt="Calline logo" width="150" height="100">
                <div class="card-body">
                  <h4 class="card-title"><b>Sistema Opensource de Desaparecidos</b></h4>
                  <p class="card-text">Sistema open-source para gerenciamento de um banco nacional aberto de pessoas desaparecidas / encontradas</p>
                  <a href="https://github.com/hemershon/desaparecidos" class="btn btn-primary">SAIBA MAIS</a>
                </div>
              </div>
            </div>
            <p>
<div class="items row">
            <div class="card-deck">
              <div class="card">
                <img class="card-img-top" src="/images/zumbi.jpg" alt="Calline logo" width="150" height="100">
                <div class="card-body">
                  <h4 class="card-title"><b>ZSSN (Zombie Survival Social Network)</b></h4>
                  <p class="card-text">Test completed for Zssn and evaluation Documentation Deliver test result Sorted out link Back-end, codeminer42</p>
                  <a href="https://github.com/hemershon/zssn" class="btn btn-primary">SAIBA MAIS</a>
                </div>
              </div>
            </div>
            <p>
<div class="items row">
            <div class="card-deck">
              <div class="card">
                <img class="card-img-top" src="/images/commerce.jpg" alt="Calline logo" width="150" height="100">
                <div class="card-body">
                  <h4 class="card-title"><b>Hello Brinde E-commerce</b></h4>
                  <p class="card-text">Primeiros passos é simples a documentação do você encontra aqui http://docs.solidus.io/</p>
                  <a href="https://github.com/hemershon/hellobrinde" class="btn btn-primary">SAIBA MAIS</a>
                </div>
              </div>
            </div>
            <p>

<div class="items row">
            <div class="card-deck">
              <div class="card">
                <img class="card-img-top" src="/images/caminhao.png" alt="Calline logo" width="150" height="100">
                <div class="card-body">
                  <h4 class="card-title"><b>TRANSPORTADORA INFOEX</b></h4>
                  <p class="card-text">Desafio Infoway, desenvolver um sistema que controle o gasto de combustível.
                  Sistema de controle de combustível que tem por finalidade o controle de gastos referentes a entrada e saída do combustível da empresa..</p>
                  <a href="https://github.com/hemershon/TransportadoraInfoEx" class="btn btn-primary">SAIBA MAIS</a>
                </div>
              </div>
            </div>
            <p>

<div class="items row">
            <div class="card-deck">
              <div class="card">
                <img class="card-img-top" src="/images/download.png" alt="Calline logo" width="150" height="100">
                <div class="card-body">
                  <h4 class="card-title"><b>Sistema Opensource Meu Diário</b></h4>
                  <p class="card-text">Sistema open-source para gerenciamento de pessoas que tenha intuito e pensamentos suicidas.</p>
                  <a href="https://github.com/hemershon/mydiary" class="btn btn-primary">SAIBA MAIS</a>
                </div>
              </div>
            </div>
            <p>


<div class="items row">
            <div class="card-deck">
              <div class="card">
                <img class="card-img-top" src="/images/agenda.png" alt="Calline logo" width="150" height="130">
                <div class="card-body">
                  <h4 class="card-title"><b>Agenda de Contatos</b></h4>
                  <p class="card-text">Desenvolvido em Ruby on Rails
Desenvolvido em RoR e o banco de dados Postgresql e composta por container do docker.
Agenda de contatos, feito para o desafio Jus.com.br</p>
                  <a href="https://github.com/hemershon/agenda" class="btn btn-primary">SAIBA MAIS</a>
                </div>
              </div>
            </div>
            <p>



<div class="items row">
            <div class="card-deck">
              <div class="card">
                <img class="card-img-top" src="/images/custodia.png" alt="Calline logo" width="200" height="50">
                <div class="card-body">
                  <h4 class="card-title"><b>Custodia Legal</b></h4>
                  <p class="card-text">Guarda Física de Documentos, fornecendo uma infraestrutura ampla e segura para o armazenamento. O cliente pode solicitar os documentos e receber em sua sede ou, se digitalizados, por email..</p>
                  <a href="https://www.hemershon.com/custodialegal.github.io/" class="btn btn-primary">SAIBA MAIS</a>
                </div>
              </div>
            </div>
            <p>
<div class="items row">
            <div class="card-deck">
              <div class="card">
                <img class="card-img-top" src="/images/STICKES17.png" alt="Calline logo" width="150" height="100">
                <div class="card-body">
                  <h4 class="card-title"><b>Calline Sekeff</b></h4>
                  <p class="card-text">Hoje me dedico a estratégias de desenvolvimento humano. E para chegar aqui a caminhada foi cheia de experiências e aprendizados. Ciências Contábeis.</p>
                  <a href="https://www.hemershon.com/callinesekeff.github.io/" class="btn btn-primary">SAIBA MAIS</a>
                </div>
              </div>
            </div>

            