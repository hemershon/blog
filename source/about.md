---
layout: page
title: Curriculum
permalink: /about/
---

<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>



<div class="jumbotron text-center ">
  <img src="/images/foto.jpg" class="img-thumbnail" alt="Hemershon" align="left" width="150" height="100"><h1>HEMERSHON SILVA</h1>
  <p>Develop web</p>

  <div class="container">        
</div>


</div>


<img class="align-self-start mr-3" src="https://png.icons8.com/ios/50/000000/resume.png" align="left" width="30" height="30">
<div class="media-body">

  <h4 class="mt-0">Perfil Profissional</h4>
  <p>
 Possuo experiência em desenvolvimento web com ruby on rails e PHP, Javascript, CSS, HTML, Mysql, Postgresql, recentemente estou trabalhando com ferramentas como Redis, e React nas minhas práticas diárias, também possuo amplos conhecimentos em servidores como Heruko, Digital Ocean.<br>

 Trabalho por meio período, no desenvolvimento do sistema administrativo da GLMPI, estou usando a linguagem Ruby on Rails, JavaScript, HTML5, CSS3 e o banco de dados PostgreSQL, estou usando o heruko como servidor de teste. <br>


Trabalho na empresa IdéiasTI como desenvolvedor web, participo de 3 Projetos.<br>
CLASSECON é um sistema de administração de condomínio nele fui coordenador por um período de 4 meses, usei ferramentas como PHP, javascript, jquery e mysql.<br>
CLASSELOC é um sistema de adminitração de aluguel de imóveis, nele sou desenvolvedor web, para melhorias e correções, usamos ferramentas como PHP, javascript, jquery, Mysql, Rest API.<br>
PLANOB é uma plataforma de aluguel de imóveis online, você pode gerenciar todo o processo do aluguel de seu imóveis, usamos ferramentas como PHP, React Native, Rest API, Mysql.<br>

Na empresa ILuxstore , trabalhei na função de marketing digital, responsável por controlar o marketing da loja, criar as postagem além de ter desenvolvido um e-commerce para loja usando spree commerce.

Trabalhei na Empresa ECM por 5 anos, lá recebi o cargo de desenvolvedor web, em três meses na função assumi um projeto em Manaus e lá fui gerente de projeto com as seguintes funções: 

Desenvolvedor web; criador de funcionalidades referentes a cada setor; deploy do sistema infodoc; também executava as migrações e orientava na instrutura sobre o Sistema através das ferramentas HTML e CSS, ruby on rails, javascript, para o deploy passenger, banco usava mysql.

Gerente de projetos; através da catalogação e higiênização de documentos, captura de imagem, controle de qualidade , organização e guarda de documentos usavamos técnica de ECM (Gerenciamento de Conteúdo Empresarial)<p>

<p>

Competências:<p>
<span class="badge badge-secudary">Ruby on Rails e Sinatra</span>

<span class="badge badge-secudary">MySQL, PostgreSQL, Redis, MongoDB</span>
<span class="badge badge-secudary">Git, Git Flow, Gitlab, Github e Bitbucket</span>
<span class="badge badge-secudary">Linux, IPTables e Shell Script</span>
<span class="badge badge-secudary"> Amazon EC2, Amazon S3 e Amazon Lambda</span>
<span class="badge badge-secudary">Zabbix, New Relic, Grafana, Prometheus e Netdata</span>
<span class="badge badge-secudary">Rundeck, Jenkins e Gitlab Runner</span>
<span class="badge badge-secudary">Desenvolvimento e Liderança INEXH</span>
<span class="badge badge-secudary">Docker, Docker Swarm, Docker Compose, Kubernetes, Rancher</span>
  </p>

<div class="media">

  <img class="align-self-start mr-3" src="https://png.icons8.com/ios/50/000000/goal.png" align="left" width="30" height="30">
  <div class="media-body">
    <h4 class="mt-0">Objetivos</h4>
    <p>Desenvolvimento Web: Front-End Ou Back-End.</p>
  </div>

  <img class="align-self-start mr-3" src="https://png.icons8.com/wired/50/000000/student-male.png" align="left" width="30" height="30">
  <div class="media-body">
    <h4 class="mt-0">Formação</h4>
    <p>Graduando em Ciência da computação</p>
  </div>
<p></p>

  <img class="align-self-start mr-3" src="https://png.icons8.com/wired/50/000000/permanent-job.png" align="left" width="30" height="30">
  <div class="media-body">
    <h4 class="mt-0">Experiências Profissionais</h4>
    <b>GLMPI</b><br/>
Desenvolvedor web<br/>
Meio período<br/>
Trabalho por meio período, no desenvolvimento do sistema administrativo da GLMPI, estou usando a linguagem Ruby on Rails, JavaScript, HTML5, CSS3 e o banco de dados PostgreSQL, estou usando o heruko como servidor de teste.<br/>
<br>

<b>IdéiasTI</b><br/>
Desenvolvedor web<br/>
Período janeiro de 2020 – o momento<br/>

Estou trabalhando como desenvolvedor web na empresa IdéiasTI na equipe PLANOB, planob e uma plataforma que administrar aluguel e venda de imóveis, minha funções é desenvolver ferramentas para a plataforma e aplicativo, usamos tecnologias como, PHP React Native, React, REST API, MySQL, doctrini, codeigniter.<br/>
<br/>

    <b>Coordenador</b><p>
       <b>IdéiasTI</b><br/>
    Período de Agosto de 2019 – janeiro de 2020<br>

    Coordenador do projeto classecon da empresa idéiasTI, classecon é um sistema de gerenciamento de condomínio e adminiastradoras de condomínios, no projeto estamos desenvolvendo novas ferramentas
    e desenvolvendo um novo aplicativo, minha rotina e planejar novas ferramentas e resolver bugs, usamos o PHP, codeigniter, javascript, doctrini, mysql.<br>
<br/>

   <b>Programador Junior</b><p>
    NORTELINK - Teresina, PI<br>
    Outubro de 2017 - Janeiro de 2018<br>

    Programador Junior Delphi participando do desenvolvimento do sistema Sincom no Delphi

<b>DesignGráfico</b><p>
iLux Store - Macapá, AP<br>
Janeiro de 2016 á Setembro de 2016<br>
iLux Store empresa de cosméticos importados situada em Macapá
Trabalhei como design gráfico e marketing da loja.<br>
Período de 19:00 as 22:00<br>
Ferramentas: Photoshop, illustrator.<p>
<b>Gerente de Projeto e Programador junior</b><br>
Ecm Tecnologia e soluções- Macapá, AP<br>
Outubro de 2012 - Junho de 2016<br>
Função de desenvolvedor de software, gerenciamento de documentos e guarda de documento, com Birô em Macapá, Manaus, Pará, Recife, Brasília e atuando em todos as cidades.<p>
<b>Soldado</b><br>
EXÉRCITO BRASILEIRO – 34º BATALHÃO INFANTARIA DE SELVA - Oiapoque, AP<br>
Fevereiro de 2006 - Fevereiro de 2009<br>
Forças Armadas- (Oiapoque-CLN) Pelo período de Fevereiro/2006 a Fevereiro/2009, na função de soldado, no
cargo de Chefe de Tecnologia da Informação.<p>

<img class="align-self-start mr-3" src="https://png.icons8.com/wired/50/000000/diploma.png" align="left" width="30" height="30">
<div class="media-body">
  <h4 class="mt-0">Cursos</h4>
<table class="table table-striped">
  <thead>
    <tr>
      <th scope="col">Cursos</th>
      <th scope="col">Periodo</th>
      <th scope="col">Cidade</th>
      <th scope="col">Escola</th>
    </tr>
  </thead>
  <tbody>

  <tr>
    <th scope="row">Bootcamp Super Full Stack</th>
    <td>Em adamento</td>
    <td>Online</td>
    <td>onebitcode</td>
  </tr>

<tr>
    <th scope="row">Docker para desenvolvedores Ruby on Rails</th>
    <td>Novembro 2018</td>
    <td>Online</td>
    <td>Udemy</td>
  </tr>

<tr>
    <th scope="row">Iniciando com Ruby e Orientação a Objetos</th>
    <td> Agosto 2018</td>
    <td>Online</td>
    <td>Udemy</td>
  </tr>
  <tr>
    <th scope="row">TDD com Ruby on Rails, Rspec e Capybara</th>
    <td>Março 2018</td>
    <td>Online</td>
    <td>Udemy</td>
  </tr>

<tr>
    <th scope="row">Curso de Programação Lucas Caton 1.0</th>
    <td>Janeiro 2018</td>
    <td>Online</td>
    <td>Udemy</td>
  </tr>
  <tr>
    <th scope="row">Docker: Ferramenta essencial para Desenvolvedores</th>
    <td>Janeiro 2018</td>
    <td>Online</td>
    <td>Udemy</td>
  </tr>

<tr>
      <th scope="row">Carreira Programador Python com Django</th>
      <td>Janeiro de 2018 em andamento</td>
      <td>Online</td>
      <td>Devmedia</td>
    </tr>

  <tr>
    <th scope="row">LÓGICAS DE PROGRAMAÇÃO</th>
    <td>Outubro de 2013</td>
    <td>São Paulo, SP</td>
    <td>ALURA</td>
  </tr>

  <tr>
    <th scope="row">LINUX COM UBUNTU: PRIMEIROS PASSOS</th>
    <td>Dezembro de 2013</td>
    <td>São Paulo, SP</td>
    <td>ALURA</td>
  </tr>

  <tr>
        <th scope="row">GIT: TRABALHO EM EQUIPE COM CONTROLE E SEGURAÇA</th>
        <td>Outubro de 2013</td>
        <td>São Paulo, SP</td>
        <td>ALURA</td>
  </tr>
  <tr>
    <th scope="row">HTML5 e CSS3</th>
    <td>Outubro de 2013</td>
    <td>São Paulo, SP</td>
    <td>Alura</td>
  </tr>
    <tr>
      <th scope="row">APRIMORANDO A POO COM RUBY</th>
      <td>Dezembro de 2013</td>
      <td>São Paulo, SP</td>
      <td>ALURA</td>
    </tr>
    <tr>
      <th scope="row">RUBY ON RAILS - AVANÇANDO A APLICAÇÃO</th>
      <td>Outubro de 2013</td>
      <td>São Paulo, SP</td>
      <td>ALURA</td>
    </tr>

  <tr>
        <th scope="row">RUBY ON RAILS - DO ZERO AO DEPLOY</th>
        <td>Outubro de 2013</td>
        <td>São Paulo, SP</td>
        <td>ALURA</td>
  </tr>

  <tr>
      <th scope="row">Curso SQL</th>
      <td>Outubro de 2013</td>
      <td>Online</td>
      <td>Devmedia</td>
    </tr>

  <tr>
      <th scope="row">Carreira Delphi</th>
      <td>Outubro de 2017 em andamento</td>
      <td>Online</td>
      <td>Devmedia</td>
    </tr>

  </tbody>
</table>
</div>

<h4>Cursos em Andamento:</h4>


<span class="badge badge-secudary">desenvolvimento responsivo</span>

<span class="badge badge-success">JavaScript</span>
<span class="badge badge-success">redis</span><p>
<span class="badge badge-success">PostgreSQL</span>
<span class="badge badge-success">PGAdmin</span>
<span class="badge badge-success"></span>
<span class="badge badge-success">Python</span>
<span class="badge badge-success">Plano de carreira</span>
<span class="badge badge-success">Inglês</span>
<span class="badge badge-success">Jekyll</span>
<span class="badge badge-success">Java</span>
<span class="badge badge-success">React</span><p>
<span class="badge badge-success">GO</span><p>



<p>

<img src="https://png.icons8.com/ios/50/000000/personal-trainer.png">
<b>Aperfeiçoamento Pessoal</b><p>
Desenvolvimento e Liderança INEXH<p>
Novembro de 2017<br>
Desenvolver uma visão diferente do mundo, onde os objetivos pessoais e profissionais possam ser alcançados de maneira sistematizada.
Desenvolver auto liderança a partir do auto conhecimento, aprimorando ainda mais suas capacidades natas e eliminando pontos fracos.
Desenvolverá capacidade de trabalhar em equipe e liderá-las, desenvolvendo assim sinergia com as pessoas do grupo, para se alcançar os resultados desejados.
Visa o desenvolvimento da liderança e dos talentos das organizações, gerando iniciativas de responsabilidade social e uma maior comunicação interna, assim como a melhoria do clima organizacional.<p>
