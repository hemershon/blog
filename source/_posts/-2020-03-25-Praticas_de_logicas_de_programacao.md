---
layout: post
title: "O que você precisa saber sobre Desenvolvimento Web"
description: "O que você precisa saber sobre Desenvolvimento Web"
date: 2020-03-25 10:39:18
comments: true
image: https://i0.wp.com/www.mercadoeconsumo.com.br/wp-content/uploads/2019/12/bigstock-Omni-Channel-Technology-Of-Onl-336037885.jpg?fit=900%2C539&ssl=1
description: "O caminho para a programação deixei várias dicas para quem está começando, mais quero separar as 3 mais importantes"
keywords: "O caminho para a programação deixei várias dicas para quem está começando, mais quero separar as 3 mais importantes"
category: Desenvolvimento web
tags:
- welcome
- desenvolvimento
- developer
- mysql
---

#  Dicas para estudos


## Uma pequena história

Resolvi ser mais ativo no meu blog, escrever mais sobre tecnologias ou até mesmo escrever uma série de como funciona o desenvolvimento web.


Iniciei nessa carreira em 2001, mais em 1998 eu me apaixonei pelo primeiro contato com a página da UOL, inclusive tenho o meu email até hoje, mais foi no site da UOL que me apaixonei pela web, no entanto, só em 2001 que comecei a desenvolver meu primeiro site para um escola de informática.

Depois deste site fiquei parado por quase 5 anos mudando de emprego e quase não tive tempo para desenvolver nem acompanhar as novidades da época, então fui para o exército e lá trabalhei como Técnico de informática por 5 anos, depois, pulando para 2012 fui contratado por uma empresa que já trabalhava com Ruby on Rails.

## Vamos ao que intessar

Bom deixando a história de lado, vamos ao que interessa, falar sobre desenvolvimento web, se você tem interesse nessa área e não sabe por onde começar, tenho umas dicas para você, no post [O caminho para a programação](http://hemershon.com/blog/2017/Caminho_das_pedras/) deixei várias dicas para quem está começando, mais quero separar as 3 mais importantes:

  ---

### **Aprender Inglês**

### **Aprender Lógica de programação**

### **Leia a documentação**

---  

## Porque aprender Inglês?

Quase todos os conteúdos que têm no mercado sobre alguma tecnologia, 99% deles são inglês, sabendo inglês você terá conteúdo em primeira mão, caso não saiba, você terá que esperar alguém se arriscar a traduzir o conteúdo para pode entender como funciona, o que certamente vai deixar você em desvantagem comparado com aquele que lê inglês, por isso foque nisso.

  

##  Porque aprender Lógica de programação?

Façamos um comparativo, por exemplo em uma obra, para construir uma casa você precisa primeiro fazer um alicerce, entender como funciona a estrutura, onde tem que colocar as colunas, dividir sala, quarto e banheiro certo?.

Pois é, e se você não souber como isso funciona sua casa vai desmoronar com facilidade, então não se empolgue em querer aprender uma nova tecnologia sem entender como funcionam as leis básicas da programação, já vi artigos falando que você não precisa aprender lógica de programação, mas acredite, vai ser um grande profissional se entender como funciona a lógica, vai sair na frente de muitos.

## Porque ler a documentação?

Vejo bastante vídeos, cursos, livros, podcast e coachs produzindo conteúdo sobre tecnologia, mas tudo que é produzido está lá na documentação de graça. Não estou falando que esses conteúdos são descartáveis, longe de mim, tem conteúdo excelente para todos os níveis, só que tenho observado que a maioria te prende, ou seja, faz você ficar dependente deles, não vejo ninguém estimulando a ativar sua própria criatividade, a criar ferramentas, a fazer você andar com suas próprias pernas, grave isso, é mais fácil você ir de Júnior para Sénior lendo a documentação do que você depender de conteúdo de terceiros.

Espero que tenham entendido e aproveitem tudo que foi dito sobre desenvolvimento web, a experiência diária erros/acertos, que essas 3 dicas são fundamentais para desenvolver e alavancar sua carreira.

Fico por aqui, até a próxima!
