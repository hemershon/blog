---
layout: post
title: "Instalação do Debian 9 no virtualbox!"
description: "Olá essa dica é para você que deseja iniciar na no mundo linux "
date: 2018-07-05 21:49:18
comments: true
image: https://s24255.pcdn.co/wp-content/uploads/2015/11/Virtualbox.png
description: "Dicas para iniciante, você que deseja aprender mais sobre Debian 9!."
keywords: "Instalação do Debian 9 no virtualbox"
category: linux
tags:
- welcome
---
<h1>Instalação do Debina 9 no virtualbox!!</h1>

Olá tudo bem, hoje iniciamos uma sequência de aulas para você que quer aprender a programar, esse vídeo é para ensinar você a preparar uma maquina virtual usando o Debian 9, como você deve ter observado não tem muito mistério.

Esse video é pra você que está cansando de compartilhar o computador da sala, sem poder instalar uma versão do Linux para aprender a programar, esse tutorial é pra você!

<div align="center"><iframe width="560" height="315" src="https://www.youtube.com/embed/q8CZx93lPHc" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe></div>

<br>
<br>Link do virtualbox e da iso do Debian 9<br>
<br><a href="https://www.virtualbox.org/wiki/Downloads">Virtualbox</a>
<br><a href="https://www.debian.org/distrib/">Debian 9</a>
